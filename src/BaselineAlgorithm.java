import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class BaselineAlgorithm implements Algorithm {

    private Map<String, Integer> dictionaryHashMap;
    private File outputEncodedDataFile;

    private long encodingTimeMillis = -1;
    private long decodingTimeMillis = -1;
    private long dictionaryBytes = -1;
    private long encodedBytes = -1;

    public BaselineAlgorithm() {
    }

    @Override
    public String getName() {
        return "Baseline";
    }

    @Override
    public void encode(File inputDataFile, File outputEncodedDataFile) {
        try {
            long start = System.currentTimeMillis();
            this.outputEncodedDataFile = outputEncodedDataFile;
            BufferedReader bufferedReader = new BufferedReader(new FileReader(inputDataFile));
            dictionaryHashMap = new HashMap<>();
            int encodedValue = 0;
            PrintWriter printWriter = new PrintWriter(outputEncodedDataFile);

            String line = bufferedReader.readLine();
            while (line != null) {
                String[] splittedLine = line.split(" ", 3);
                for (String word : splittedLine) {
                    String timmedWord = word.trim();
                    if (!dictionaryHashMap.containsKey(timmedWord)) {
                        encodedValue++;
                        dictionaryHashMap.put(timmedWord, encodedValue);
                    }
                    printWriter.append(String.valueOf(dictionaryHashMap.get(timmedWord))).append(" ");
                }
                printWriter.append("\r\n");
                line = bufferedReader.readLine();
            }
            printWriter.close();
            bufferedReader.close();
            encodedBytes = outputEncodedDataFile.length();
            encodingTimeMillis = System.currentTimeMillis() - start;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveDictionary(File outputDictionaryFile) {
        try {
            PrintWriter printWriter = new PrintWriter(outputDictionaryFile);
            for (Map.Entry<String, Integer> entry : dictionaryHashMap.entrySet()) {
                String word = entry.getKey();
                int encodedWord = entry.getValue();
                printWriter.write(word + " ");
                printWriter.append(String.valueOf(encodedWord)).append(" ");
                printWriter.append("\r\n");
            }
            printWriter.close();
            dictionaryBytes = outputDictionaryFile.length();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void decode(File outputDecodedDataFile) {
        try {
            long start = System.currentTimeMillis();
            PrintWriter printWriter = new PrintWriter(outputDecodedDataFile);
            Map<Integer, String> reverseDictionary = new HashMap<>();
            for (Map.Entry<String, Integer> entry : dictionaryHashMap.entrySet()) {
                reverseDictionary.put(entry.getValue(), entry.getKey());
            }

            BufferedReader bufferedReader = new BufferedReader(new FileReader(outputEncodedDataFile));
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] encodedWords = line.split(" ", 3);
                for (String encodedWord : encodedWords) {
                    String word = reverseDictionary.get(Integer.parseInt(encodedWord.trim()));
                    printWriter.append(word).append(" ");
                }
                printWriter.append("\r\n");
                line = bufferedReader.readLine();
            }
            printWriter.close();
            bufferedReader.close();
            decodingTimeMillis = System.currentTimeMillis() - start;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public long getEncodingTime() {
        return encodingTimeMillis;
    }

    @Override
    public long getDecodingTime() {
        return decodingTimeMillis;
    }

    @Override
    public long getDictionaryBytes() {
        return dictionaryBytes;
    }

    @Override
    public long getEncodedBytes() {
        return encodedBytes;
    }

    @Override
    public void clear() {
        dictionaryHashMap.clear();
        System.gc();
    }
}
