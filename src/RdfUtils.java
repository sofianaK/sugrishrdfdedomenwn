import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFDataMgr;

import java.io.PrintWriter;

public class RdfUtils {

    public static final int TRIPLETS_ALL = -1;

    public static void rdtToTriplets(String rdfInputFileName, String outputFileName, int maxTiplets) throws Exception {

        Model model = RDFDataMgr.loadModel(rdfInputFileName);

        // list the statements in the Model
        StmtIterator iter = model.listStatements();

        int index = 0;
        PrintWriter printWriter = new PrintWriter(outputFileName);
        while (iter.hasNext() && (maxTiplets == TRIPLETS_ALL || index < maxTiplets)) {
            index++;
            Statement stmt = iter.nextStatement();  // get next statement
            Resource subject = stmt.getSubject();     // get the subject
            Property predicate = stmt.getPredicate();   // get the predicate
            RDFNode object = stmt.getObject();      // get the object
            String line;
            String objectStr = object.toString().replace("\n", "").replace("\r", "");
            if (object instanceof Resource) {
                line = subject.toString() + " " + predicate.toString() + " " + objectStr;
            } else {
                line = subject.toString() + " " + predicate.toString() + " \"" + objectStr + "\"";
            }
            printWriter.write(line);
            printWriter.append("\r\n");
        }
        printWriter.close();
    }

}
