import java.io.File;
import java.io.IOException;

public interface Algorithm {

    String getName();

    //Apothikeuw to dictionary
    void encode(File inputDataFile, File outputEncodedDataFile);

    //Grafw se arxeio to disctionary
    void saveDictionary(File outputDictionaryFile);

    //Apothikeuw to reverse dictionary
    void decode(File outpuDecodedDataFile);

    long getEncodingTime();

    long getDecodingTime();

    long getDictionaryBytes();

    long getEncodedBytes();

    void clear();
}
