import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        // Diavazw to txt me ta rdf dedomena
        File parentDirectory = new File("results");
        FileUtils.deleteDirectory(parentDirectory);
        parentDirectory.mkdirs();
        List<String> inputDataList = new ArrayList<>();

        //https://old.datahub.io/dataset/eventskg/resource/91c0f744-9d6e-4076-b604-b7c36b29d9bc
        System.out.println("Converting scientificEvents.ttl to tiplets...");
        RdfUtils.rdtToTriplets("rdfTestInputs/scientificEvents.ttl", "results/scientificEvents_input.txt", RdfUtils.TRIPLETS_ALL);
        inputDataList.add("results/scientificEvents");
        System.out.println("Converting scientificEvents.ttl to tiplets... success");

        //https://old.datahub.io/dataset/unescothes/resource/2f31bdd8-b243-4a8d-b360-cfa804aec66a
        System.out.println("Converting unescothes.ttl to tiplets...");
        RdfUtils.rdtToTriplets("rdfTestInputs/unescothes.ttl", "results/unescothes_input.txt", RdfUtils.TRIPLETS_ALL);
        inputDataList.add("results/unescothes");
        System.out.println("Converting unescothes.ttl to tiplets... success");

        //https://old.datahub.io/dataset/sli-galnet-rdf
        System.out.println("Converting rdf_galnet_ili2monolingual.ttl to tiplets...");
        RdfUtils.rdtToTriplets("rdfTestInputs/rdf_galnet_ili2monolingual.ttl", "results/rdf_galnet_ili2monolingual_input.txt", RdfUtils.TRIPLETS_ALL);
        inputDataList.add("results/rdf_galnet_ili2monolingual");
        System.out.println("Converting rdf_galnet_ili2monolingual.ttl to tiplets... success");

        System.out.println();
        System.out.println();
        for (String inputData : inputDataList) {
            System.out.println("########################### " + inputData + " ###########################");
            System.out.println();
            System.out.println("Original file size in bytes -> " + new File(inputData + "_input.txt").length());
            System.out.println();

            AlgorithmComparison comparison = new AlgorithmComparison(inputData);
            comparison.addAlgorithmForCompare(new BaselineAlgorithm());
            comparison.addAlgorithmForCompare(new HuffmanAlgorithm());
            comparison.addAlgorithmForCompare(new LZWAlgorithm());
            comparison.compare();

            System.out.println("######################################################");
            System.out.println();
            System.out.println();
        }
    }
}
