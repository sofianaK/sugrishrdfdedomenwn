import java.io.*;
import java.util.HashMap;
import java.util.Map;

//LZW Algorithm
public class LZWAlgorithm implements Algorithm {

    private static int DICTIONARY_SIZE = 2048;

    private Map<String, Integer> dictionary;
    private File outputEncodedDataFile;

    private long encodingTimeMillis = -1;
    private long decodingTimeMillis = -1;
    private long dictionaryBytes = -1;
    private long encodedBytes = -1;

    @Override
    public String getName() {
        return "LZW";
    }

    @Override
    public void encode(File inputDataFile, File outputEncodedDataFile) {
        try {
            long start = System.currentTimeMillis();
            this.outputEncodedDataFile = outputEncodedDataFile;

            PrintWriter printWriter = new PrintWriter(outputEncodedDataFile);
            BufferedReader bufferedReader = new BufferedReader(new FileReader(inputDataFile));
            // Build the dictionary.
            int index = DICTIONARY_SIZE;
            dictionary = new HashMap<>();
            for (int i = 0; i < DICTIONARY_SIZE; i++) {
                dictionary.put("" + (char) i, i);
            }
            String w = "";
            String line = bufferedReader.readLine();
            line = line + "\n";
            while (line != null) {
                for (char c : line.toCharArray()) {
                    String wc = w + c;
                    if (dictionary.containsKey(wc)) {
                        w = wc;
                    } else {
                        int i = dictionary.get(w);
                        printWriter.append((char) i);
                        // Add wc to the dictionary.
                        dictionary.put(wc, index++);
                        w = "" + c;
                    }
                }
                line = bufferedReader.readLine();
                if(line != null) {
                    line = line + "\n";
                }
            }

            // Output the code for w.
            if (!w.equals("")) {
                int i = dictionary.get(w);
                printWriter.append((char) i);
            }

            bufferedReader.close();
            printWriter.close();
            encodedBytes = outputEncodedDataFile.length();
            encodingTimeMillis = System.currentTimeMillis() - start;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveDictionary(File outputDictionaryFile) {
        PrintWriter printWriter;
        try {
            printWriter = new PrintWriter(outputDictionaryFile);
            for (Map.Entry<String, Integer> entry : dictionary.entrySet()) {
                String word = entry.getKey();
                int encodedWord = entry.getValue();
                printWriter.write(word + " ");
                printWriter.append(String.valueOf(encodedWord)).append(" ");
                printWriter.append("\n");
            }
            printWriter.close();
            dictionaryBytes = outputDictionaryFile.length();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void decode(File outputDecodedDataFile) {
        try {
            long start = System.currentTimeMillis();
            PrintWriter printWriter = new PrintWriter(outputDecodedDataFile);

            // Build the dictionary.
            BufferedReader bufferedReader = new BufferedReader(new FileReader(outputEncodedDataFile));
            int index = DICTIONARY_SIZE;
            Map<Integer, String> dictionary = new HashMap<>();
            for (int i = 0; i < DICTIONARY_SIZE; i++) {
                dictionary.put(i, "" + (char) i);
            }
            int character = bufferedReader.read();
            String w = "" + (char) character;
            printWriter.append(w);
            while ((character = bufferedReader.read()) != -1) {
                int k = (int) character;
                String entry;
                if (dictionary.containsKey(k)) {
                    entry = dictionary.get(k);
                } else if (k == index) {
                    entry = w + w.charAt(0);
                } else {
                    throw new IllegalArgumentException("Bad compressed k: " + k);
                }
                printWriter.append(entry);

                // Add w+entry[0] to the dictionary.
                dictionary.put(index++, w + entry.charAt(0));
                w = entry;
            }
            bufferedReader.close();
            printWriter.close();
            decodingTimeMillis = System.currentTimeMillis() - start;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public long getEncodingTime() {
        return encodingTimeMillis;
    }

    @Override
    public long getDecodingTime() {
        return decodingTimeMillis;
    }

    @Override
    public long getDictionaryBytes() {
        return dictionaryBytes;
    }

    @Override
    public long getEncodedBytes() {
        return encodedBytes;
    }

    @Override
    public void clear() {
        dictionary.clear();
        System.gc();
    }
}
