import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class HuffmanAlgorithm implements Algorithm {

    private static final int ALPHABET_SIZE = 2048;

    private Map<Character, String> lookupTable;
    private int[] characterFrequencies;
    private Node huffmanTree;
    private File outputEncodedDataFile;

    private long encodingTimeMillis = -1;
    private long decodingTimeMillis = -1;
    private long dictionaryBytes = -1;
    private long encodedBytes = -1;

    public HuffmanAlgorithm() {
    }

    @Override
    public String getName() {
        return "Huffman";
    }

    private String compress(String input) {
        if (input == null) {
            return "";
        }
        return input.length() - 1 + "";
    }

    private String decompress(String input) {
        if (input == null) {
            return "";
        }
        int leadingZeroes = Integer.parseInt(input.trim());
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < leadingZeroes; i++) {
            sb.append("0");
        }
        sb.append("1");
        return sb.toString();
    }

    @Override
    public void encode(File inputDataFile, File outputEncodedDataFile) {
        try {
            long start = System.currentTimeMillis();

            this.outputEncodedDataFile = outputEncodedDataFile;
            characterFrequencies = buildFrequenciesTable(new BufferedReader(new FileReader(inputDataFile)));
            huffmanTree = buildHuffmanTree(characterFrequencies);
            lookupTable = buildLookupTable(huffmanTree);

            PrintWriter printWriter = new PrintWriter(outputEncodedDataFile);
            BufferedReader bufferedReader = new BufferedReader(new FileReader(inputDataFile));
            String line = bufferedReader.readLine();
            encodedBytes = 0;
            while (line != null) {
                for (char character : line.toCharArray()) {
                    String encodedCharStr = lookupTable.get(character);
                    encodedBytes += encodedCharStr.length();
                    printWriter.append(compress(encodedCharStr));
                    printWriter.append(" ");
                }
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
            printWriter.close();

            encodedBytes = encodedBytes / 8;
            encodingTimeMillis = System.currentTimeMillis() - start;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveDictionary(File outputDictionaryFile) {
        PrintWriter printWriter;
        try {
            printWriter = new PrintWriter(outputDictionaryFile);
            for (Map.Entry<Character, String> entry : lookupTable.entrySet()) {
                char word = entry.getKey();
                String encodedWord = entry.getValue();
                printWriter.write(word + " ");
                printWriter.append(encodedWord).append(" ");
                printWriter.append("\r\n");
            }
            printWriter.close();
            dictionaryBytes = outputDictionaryFile.length();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void decode(File outputDecodedDataFile) {
        try {
            long start = System.currentTimeMillis();
            PrintWriter printWriter = new PrintWriter(outputDecodedDataFile);
            Node current = huffmanTree;

            BufferedReader bufferedReader = new BufferedReader(new FileReader(outputEncodedDataFile));

            int ch;
            StringBuilder sb = new StringBuilder();
            while ((ch = bufferedReader.read()) != -1) {
                if (ch != ' ') {
                    sb.append((char) ch);
                    continue;
                }
                String byteString = decompress(sb.toString());
                sb = new StringBuilder();
                BufferedReader bf = new BufferedReader(new StringReader(byteString));
                int character;
                while ((character = bf.read()) != -1) {
                    boolean skipRead = true;
                    while (!current.isLeaf()) {
                        if (!skipRead) {
                            character = bf.read();
                        }
                        skipRead = false;
                        if (character == -1) {
                            break;
                        }
                        char bit = (char) character;
                        if (bit == '1') {
                            current = current.rightChild;
                        } else if (bit == '0') {
                            current = current.leftChild;
                        } else {
                            throw new IllegalArgumentException("Invalid bit in message! " + bit);
                        }
                    }
                    printWriter.append(current.nodeCharacter);
                    current = huffmanTree;
                }
                bf.close();
            }
            printWriter.close();
            bufferedReader.close();
            decodingTimeMillis = System.currentTimeMillis() - start;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public long getEncodingTime() {
        return encodingTimeMillis;
    }

    @Override
    public long getDecodingTime() {
        return decodingTimeMillis;
    }

    @Override
    public long getDictionaryBytes() {
        return dictionaryBytes;
    }

    @Override
    public long getEncodedBytes() {
        return encodedBytes;
    }

    @Override
    public void clear() {
        lookupTable.clear();
        characterFrequencies = null;
        huffmanTree = null;
        System.gc();
    }

    private int[] buildFrequenciesTable(BufferedReader bufferedReader) throws IOException {
        int[] frequenciesTable = new int[ALPHABET_SIZE];
        String line = bufferedReader.readLine();
        while (line != null) {
            for (char character : line.toCharArray()) {
                frequenciesTable[character]++;
            }
            line = bufferedReader.readLine();
        }
        bufferedReader.close();
        return frequenciesTable;
    }

    private Node buildHuffmanTree(int[] frequencies) {
        PriorityQueue<Node> priorityQueue = new PriorityQueue<>();
        for (char i = 0; i < ALPHABET_SIZE; i++) {
            if (frequencies[i] > 0) {
                priorityQueue.add(new Node(i, frequencies[i]));
            }
        }
//        if (priorityQueue.size() == 1) {
//            priorityQueue.add(new Node('\0', 1));
//        }
        while (priorityQueue.size() > 1) {
            Node left = priorityQueue.poll();
            Node right = priorityQueue.poll();
            Node parent = new Node(left.nodeFrequency + right.nodeFrequency, left, right);
            priorityQueue.add(parent);
        }
        return priorityQueue.poll();
    }

    private Map<Character, String> buildLookupTable(Node root) {
        Map<Character, String> lookupTable = new HashMap<>();
        buildLookupTableRecursive(root, "", lookupTable);
        return lookupTable;
    }

    private void buildLookupTableRecursive(Node node, String s, Map<Character, String> lookupTable) {
        if (!node.isLeaf()) {
            buildLookupTableRecursive(node.leftChild, s + "0", lookupTable);
            buildLookupTableRecursive(node.rightChild, s + "1", lookupTable);
        } else {
            lookupTable.put(node.nodeCharacter, s);
        }
    }

    static class Node implements Comparable<Node> {
        private final char nodeCharacter;
        private final int nodeFrequency;
        private final Node leftChild;
        private final Node rightChild;

        private Node(char nodeCharacter, int nodeFrequency, Node leftChild, Node rightChild) {
            this.nodeCharacter = nodeCharacter;
            this.nodeFrequency = nodeFrequency;
            this.leftChild = leftChild;
            this.rightChild = rightChild;
        }

        private Node(char nodeCharacter, int nodeFrequency) {
            this(nodeCharacter, nodeFrequency, null, null);
        }

        private Node(int nodeFrequency, Node leftChild, Node rightChild) {
            this('\0', nodeFrequency, leftChild, rightChild);
        }

        boolean isLeaf() {
            return this.leftChild == null && this.rightChild == null;
        }

        @Override
        public int compareTo(Node o) {
            return Integer.compare(this.nodeFrequency, o.nodeFrequency);
        }
    }
}

