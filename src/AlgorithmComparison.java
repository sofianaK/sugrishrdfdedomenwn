import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AlgorithmComparison {

    private List<Algorithm> algorithmsToCompare;
    private String inputDataName;
    private File inputFile;

    public AlgorithmComparison(String inputDataName) {
        algorithmsToCompare = new ArrayList<>();
        this.inputDataName = inputDataName;
        this.inputFile = new File(inputDataName + "_input.txt");
    }

    public void addAlgorithmForCompare(Algorithm algorithm) {
        algorithmsToCompare.add(algorithm);
    }

    public void compare() {
        for (Algorithm algorithm : algorithmsToCompare) {
            System.out.println("-------------------- " + algorithm.getName() + " --------------------");

            File dictionaryFile = new File(inputDataName + "_" + algorithm.getName() + "_dictionary.txt");
            File encodedFile = new File(inputDataName + "_" + algorithm.getName() + "_encoded.txt");
            File decodedFile = new File(inputDataName + "_" + algorithm.getName() + "_decoded.txt");

            algorithm.encode(inputFile, encodedFile);
            System.out.println("Encoding time in milliseconds -> " + algorithm.getEncodingTime());
            algorithm.decode(decodedFile);
            System.out.println("Decoding time in milliseconds -> " + algorithm.getDecodingTime());
            algorithm.saveDictionary(dictionaryFile);
            System.out.println("Dictionary file size in bytes -> " + algorithm.getDictionaryBytes());
            System.out.println("Encoded file size in bytes -> " + algorithm.getEncodedBytes());
            algorithm.clear();

            System.out.println("-----------------------------------------");
            System.out.println();
        }

    }
}
